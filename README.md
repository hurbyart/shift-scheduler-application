# Shift Scheduler

Shift Scheduler is course work of Enterprise Architecture subject. The application was created by Artem Hurbych (hurbyart@fel.cvut.cz) and Daria Dunina (dunindar@fel.cvut.cz). Only backend of application is implemented. Functionality can be tested using Postman or analogic application.

The application is created for manager of bars/restaurants/other business with shift work to automatize creation of shift schedule and to make urgent communication easier.
