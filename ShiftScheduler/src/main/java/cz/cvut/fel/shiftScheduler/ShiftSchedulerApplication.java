package cz.cvut.fel.shiftScheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The application is created for staff of bars/restaurants/other business with shift work to automatize creation of shift schedule
 * and to make urgent communication easier.
 * Main entry point of a Spring Boot application.
 *
 * @author Artem Hurbych (hurbyart@fel.cvut.cz)
 * @author Daria Dunina (dunindar@fel.cvut.cz)
 */
@SpringBootApplication
@EnableAutoConfiguration
public class ShiftSchedulerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiftSchedulerApplication.class, args);
    }
}
