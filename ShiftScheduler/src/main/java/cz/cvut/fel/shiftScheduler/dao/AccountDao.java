package cz.cvut.fel.shiftScheduler.dao;

import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Objects;

@Repository
public class AccountDao extends BaseDao<Account> {

    public AccountDao() {
        super(Account.class);
    }

    public Account findByUsername(String username) {
        Objects.requireNonNull(username);
        try {
            List<Account> accountList = em.createNamedQuery("Account.findByUsername", Account.class).setParameter("username", username).getResultList();
            if (accountList.isEmpty()) return null;
            return accountList.get(0);
        } catch (NoResultException e) {
            return null;
        }
    }

    public Account findByEmail(String email) {
        Objects.requireNonNull(email);
        try {
            List<Account> accountList = em.createNamedQuery("Account.findByEmail", Account.class).setParameter("email", email).getResultList();
            if (accountList.isEmpty()) return null;
            return accountList.get(0);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Account> findOnlyWorkers() {
        try {
            List<Account> accountList = em.createNamedQuery("Account.findWorkers", Account.class).setParameter("role", Role.WORKER).getResultList();
            return accountList;
        } catch (NoResultException e) {
            return null;
        }
    }
}
