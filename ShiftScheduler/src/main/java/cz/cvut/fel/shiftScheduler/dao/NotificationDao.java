package cz.cvut.fel.shiftScheduler.dao;

import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Notification;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Objects;

@Repository
public class NotificationDao extends BaseDao<Notification> {

    public NotificationDao(){
        super(Notification.class);
    }

    public List<Notification> findByAccount(Account account){
        Objects.requireNonNull(account);
        return em.createNamedQuery("Notification.findByAccount", Notification.class).setParameter("account",account).getResultList();
    }
}
