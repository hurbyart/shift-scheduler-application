package cz.cvut.fel.shiftScheduler.dao;

import cz.cvut.fel.shiftScheduler.model.PollVote;
import org.springframework.stereotype.Repository;

@Repository
public class PollVoteDao extends BaseDao<PollVote> {

    public PollVoteDao() {
        super(PollVote.class);
    }
}
