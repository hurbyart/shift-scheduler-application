package cz.cvut.fel.shiftScheduler.dao;

import cz.cvut.fel.shiftScheduler.model.Schedule;
import cz.cvut.fel.shiftScheduler.model.Shift;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class ScheduleDao extends BaseDao<Schedule> {

    public ScheduleDao() {
        super(Schedule.class);
    }

    @Override
    public Schedule find(Integer id) {
        Objects.requireNonNull(id);
        List<Schedule> list = em.createNamedQuery("Schedule.findById", Schedule.class).setParameter("id", id).getResultList();
        if(list == null){
            return null;
        }
        else{
            return list.get(0);
        }

    }
}
