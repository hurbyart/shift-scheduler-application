package cz.cvut.fel.shiftScheduler.dao;

import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Shift;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class ShiftDao extends BaseDao<Shift> {

    public ShiftDao() {
        super(Shift.class);
    }

    public List<Shift> findAllByDate(Date date) {
        Objects.requireNonNull(date);
        return em.createNamedQuery("Shift.findAllByDate", Shift.class).setParameter("date", date).getResultList();
    }

    public List<Shift> findAllByWorker(Account worker) {
        Objects.requireNonNull(worker);
        return em.createNamedQuery("Shift.findAllByWorker", Shift.class).setParameter("worker", worker).getResultList();
    }
}