package cz.cvut.fel.shiftScheduler.dao;

import cz.cvut.fel.shiftScheduler.model.PollVote;
import cz.cvut.fel.shiftScheduler.model.ShiftPoll;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ShiftPollDao extends BaseDao<ShiftPoll> {

    public ShiftPollDao() {
        super(ShiftPoll.class);
    }

    public PollVote findVoteByUsername(ShiftPoll poll, String username) {
        List<PollVote> votes = poll.getVotes();
        for (PollVote vote : votes) {
            if (vote.getWorkerAccount().getUsername().equals(username))
                return vote;
        }
        return null;
    }
}