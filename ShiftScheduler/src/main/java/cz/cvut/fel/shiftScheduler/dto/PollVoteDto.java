package cz.cvut.fel.shiftScheduler.dto;

import cz.cvut.fel.shiftScheduler.model.AnswerType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class PollVoteDto {

    private String remark;

    @Enumerated(EnumType.STRING)
    AnswerType answerType;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }
}
