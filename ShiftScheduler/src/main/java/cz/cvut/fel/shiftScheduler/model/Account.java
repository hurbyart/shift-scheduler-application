package cz.cvut.fel.shiftScheduler.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "Account.findByUsername", query = "SELECT a from Account a WHERE :username = a.username"),
        @NamedQuery(name = "Account.findByEmail", query = "SELECT a from Account a WHERE :email = a.email"),
        @NamedQuery(name = "Account.findWorkers", query = "SELECT a from Account a WHERE :role = a.role")
})

public class Account extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    private String firstName;

    @Basic(optional = false)
    @Column(nullable = false)
    private String lastName;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String username;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String email;

    @Basic(optional = false)
    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void erasePassword() {
        this.password = null;
    }

    public boolean isManager() {
        return role == Role.MANAGER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return username.equals(account.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return "User{" +
                firstName + " " + lastName +
                "(" + username + ")}";
    }

    @Override
    public void copy(AbstractEntity toCopy) {
        if (toCopy instanceof Account) {
            Account newData = (Account) toCopy;
            this.firstName = newData.firstName;
            this.lastName = newData.lastName;
            this.email = newData.email;
            this.password = newData.password;
            this.role = newData.role;
            this.username = newData.username;
        }
    }
}
