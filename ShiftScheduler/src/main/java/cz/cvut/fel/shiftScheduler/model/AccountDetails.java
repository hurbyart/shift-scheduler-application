package cz.cvut.fel.shiftScheduler.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;


public class AccountDetails implements UserDetails {

    private String username;
    private String password;
    private boolean active;
    private Set<GrantedAuthority> authorities;

    public AccountDetails(Account account) {
        this.username = account.getUsername();
        this.active = true;
        this.password = account.getPassword();
        this.authorities = new HashSet<>();
        addUserRole(account);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableCollection(authorities);
    }

    public void addUserRole(Account account) {
        this.authorities.add(new SimpleGrantedAuthority(account.getRole().getName()));
    }


    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
