package cz.cvut.fel.shiftScheduler.model;

public enum AnswerType {
    YES("YES"), POSSIBLE("POSSIBLE"), NO("NO");

    private final String name;

    AnswerType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
