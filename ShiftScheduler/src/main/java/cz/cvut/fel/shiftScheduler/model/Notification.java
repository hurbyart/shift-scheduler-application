package cz.cvut.fel.shiftScheduler.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Notification.findByAccount", query = "SELECT n from Notification n WHERE :account MEMBER OF n.accounts")
})
public class Notification extends AbstractEntity{

    @Basic(optional = false)
    @Column(nullable = false)
    private String headerText;

    @Basic(optional = false)
    @Column(nullable = false)
    private String messageText;

    @Basic(optional = false)
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationType notificationType;

    @ManyToMany
    @OrderBy("username")
    private List<Account> accounts = new ArrayList<>();

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public void copy(AbstractEntity toCopy) {
        if(toCopy instanceof Notification){
            Notification newData = (Notification)toCopy;
            this.headerText = newData.headerText;
            this.messageText = newData.messageText;
            this.notificationType = newData.notificationType;
        }
    }
}
