package cz.cvut.fel.shiftScheduler.model;

public enum NotificationType {

    NEW_POLL("NOTIFICATION_NEW_POLL"), FREE_SHIFT_SLOT("NOTIFICATION_FREE_SHIFT_SLOT"),
    SHIFT_CANCEL("NOTIFICATION_SHIFT_CANCEL"), INFORMATION("NOTIFICATION_INFORMATION");

    private final String name;

    NotificationType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
