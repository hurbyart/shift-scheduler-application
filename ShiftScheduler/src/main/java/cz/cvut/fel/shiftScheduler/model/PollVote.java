package cz.cvut.fel.shiftScheduler.model;

import cz.cvut.fel.shiftScheduler.dto.PollVoteDto;

import javax.persistence.*;

@Entity
public class PollVote extends AbstractEntity {

    private String remark;

    @Enumerated(EnumType.STRING)
    private AnswerType answerType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Account workerAccount;

    public PollVote() {

    }

    public PollVote(PollVoteDto dto, Account workerAccount) {
        this.remark = dto.getRemark();
        this.answerType = dto.getAnswerType();
        this.workerAccount = workerAccount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }

    public Account getWorkerAccount() {
        return workerAccount;
    }

    public void setWorkerAccount(Account workerAccount) {
        this.workerAccount = workerAccount;
    }

    @Override
    public void copy(AbstractEntity toCopy) {
        if (toCopy instanceof PollVote) {
            PollVote newData = (PollVote) toCopy;
            this.answerType = newData.answerType;
        }
    }
}
