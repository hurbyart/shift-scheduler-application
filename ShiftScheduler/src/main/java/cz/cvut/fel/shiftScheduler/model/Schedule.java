package cz.cvut.fel.shiftScheduler.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Schedule.findById", query = "SELECT s from Schedule s WHERE :id = s.id")
})
@IdClass(ScheduleId.class)
public class Schedule extends AbstractEntity {

    @GeneratedValue
    @Column(nullable = false, unique = true)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id()
    private String scheduleName;

    @Id
    private Date fromDate;

    @Basic(optional = false)
    @Column(nullable = false)
    private Date toDate;

    @OneToMany
    @OrderBy("dateStart")
    private List<Shift> shifts = new ArrayList<>();

    public String getName() {
        return scheduleName;
    }

    public void setName(String name) {
        this.scheduleName = name;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<Shift> getShifts() {
        return shifts;
    }

    public void setShifts(List<Shift> shifts) {
        this.shifts = shifts;
    }

    public Schedule() {

    }

    public Schedule(String name, List<ShiftPoll> polls) {
        this.scheduleName = name;

        fillSchedule(polls);
        updateDates();
    }

    public void addShift(Shift toAdd) {
        if (!shifts.contains(toAdd))
            shifts.add(toAdd);
    }

    public boolean removeShift(Shift toRemove) {
        if(shifts.remove(toRemove)){
            if(toRemove.getDateStart().equals(this.toDate)){
                this.updateDates();
            }
            return true;
        }
        return false;
    }

    @Override
    public void copy(AbstractEntity toCopy) {
        if (toCopy instanceof Schedule) {
            Schedule newData = (Schedule) toCopy;
            this.scheduleName = newData.scheduleName;
            this.fromDate = newData.fromDate;
            this.toDate = newData.toDate;
        }
    }

    private void updateDates() {
        Date min = shifts.get(0).getDateStart();
        Date max = shifts.get(0).getDateStart();
        for (Shift shift : shifts) {
            Date shiftDate = shift.getDateStart();
            if (shiftDate.after(max))
                max = shiftDate;
            if (shiftDate.before(min))
                min = shiftDate;
        }

        this.fromDate = min;
        this.toDate = max;
    }

    private void fillSchedule(List<ShiftPoll> polls) {
        HashMap<Integer, Integer> workerId_shiftCountMap = new HashMap<>();
        for (ShiftPoll poll : polls) {

            List<PollVote> votes = poll.getVotes();
            Shift pollShift = poll.getShift();
            shifts.add(pollShift);

            List<PollVote> candidates = new ArrayList<>();

            for (PollVote vote : votes) {
                if (vote.getAnswerType() == AnswerType.NO) { //worker can't take the shift
                    continue;
                }

                if (candidates.size() < pollShift.getCapacity()) {
                    candidates.add(vote);
                } else {
                    if (changePossibleToYes(candidates, vote)) {
                        continue;
                    }
                    changeMoreShiftsToLess(candidates, vote, workerId_shiftCountMap);
                }
            }

            pollShift.setAssignedAccountsFromVotes(candidates);
            updateHashMap(candidates, workerId_shiftCountMap);
        }
    }

    private void updateHashMap(List<PollVote> candidates, HashMap<Integer, Integer> workerId_shiftCountMap) {
        for (PollVote vote : candidates) {
            Integer workerId = vote.getWorkerAccount().getId();

            if (workerId_shiftCountMap.containsKey(workerId)) {
                workerId_shiftCountMap.put(workerId, workerId_shiftCountMap.get(workerId) + 1);
            } else {
                workerId_shiftCountMap.put(workerId, 1);
            }
        }
    }

    private Boolean changePossibleToYes(List<PollVote> candidates, PollVote newCandidate) {
        if (newCandidate.getAnswerType() == AnswerType.POSSIBLE) {
            return false;
        }
        for (PollVote candidate : candidates) {
            if (candidate.getAnswerType() == AnswerType.POSSIBLE) {
                candidates.remove(candidate);
                candidates.add(newCandidate);
                return true;
            }
        }
        return false;
    }

    private void changeMoreShiftsToLess(List<PollVote> candidates, PollVote newCandidate, HashMap<Integer, Integer> workerId_shiftCountMap) {
        Integer newCandidateCount = workerId_shiftCountMap.get(newCandidate.getWorkerAccount().getId());
        newCandidateCount = newCandidateCount == null ? 0 : newCandidateCount;

        for (PollVote candidate : candidates) {
            Integer candidateCount = workerId_shiftCountMap.get(candidate.getWorkerAccount().getId());
            candidateCount = candidateCount == null ? 0 : candidateCount;

            if (newCandidateCount < candidateCount) {
                candidates.remove(candidate);
                candidates.add(newCandidate);
                break;
            }
        }
    }
}
