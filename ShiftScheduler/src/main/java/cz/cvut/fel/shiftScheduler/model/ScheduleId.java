package cz.cvut.fel.shiftScheduler.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class ScheduleId implements Serializable {

    private String scheduleName;
    private Date fromDate;

    public ScheduleId() {
    }

    public ScheduleId(String name, Date fromDate) {
        this.scheduleName = name;
        this.fromDate = fromDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduleId that = (ScheduleId) o;
        return scheduleName.equals(that.scheduleName) &&
                fromDate.equals(that.fromDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheduleName, fromDate);
    }
}
