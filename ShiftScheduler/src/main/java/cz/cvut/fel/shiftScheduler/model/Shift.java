package cz.cvut.fel.shiftScheduler.model;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "Shift.findAllByDate", query = "SELECT s from Shift s WHERE :date = s.dateStart"),
        @NamedQuery(name = "Shift.findAllByWorker", query = "SELECT s from Shift s WHERE :worker MEMBER OF s.assignedAccounts")
})
public class Shift extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    private String shiftName;

    @Basic(optional = false)
    @Column(nullable = false)
    private Time fromTime;

    @Basic(optional = false)
    @Column(nullable = false)
    private Time toTime;

    @Basic(optional = false)
    @Column(nullable = false)
    private Date dateStart;

    @Basic(optional = false)
    @Column(nullable = false)
    private Integer capacity;

    @ManyToMany
    private List<Account> assignedAccounts = new ArrayList<>();

    public Time getFromTime() {
        return fromTime;
    }

    public void setFromTime(Time fromTime) {
        this.fromTime = fromTime;
    }

    public Time getToTime() {
        return toTime;
    }

    public void setToTime(Time toTime) {
        this.toTime = toTime;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public List<Account> getAssignedAccounts() {
        return assignedAccounts;
    }

    public void setAssignedAccounts(List<Account> assignedAccounts) {
        this.assignedAccounts = assignedAccounts;
    }

    public void setAssignedAccountsFromVotes(List<PollVote> votes) {
        this.assignedAccounts.clear();

        for (PollVote vote : votes) {
            this.assignedAccounts.add(vote.getWorkerAccount());
        }
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void assignWorker(Account worker) {
        Objects.requireNonNull(worker);
        if (!assignedAccounts.contains(worker))
            assignedAccounts.add(worker);
    }

    public boolean cancelWorker(Account worker) {
        Objects.requireNonNull(worker);
        return assignedAccounts.remove(worker);
    }

    @Override
    public void copy(AbstractEntity toCopy) {
        if (toCopy instanceof Shift) {
            Shift newData = (Shift) toCopy;
            this.shiftName = newData.shiftName;
            this.fromTime = newData.fromTime;
            this.toTime = newData.toTime;
            this.dateStart = newData.dateStart;
        }
    }

}
