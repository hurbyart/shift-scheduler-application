package cz.cvut.fel.shiftScheduler.model;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class ShiftPoll extends AbstractEntity {

    @Basic
    @Column
    private Time timeDue;

    @Basic
    @Column
    private Date dateDue;

    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean open;

    @OneToMany(orphanRemoval = true)
    private List<PollVote> votes = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username")
    private Account username;

    @OneToOne
    private Shift shift;

    public ShiftPoll() {
        this.open = true;
    }

    public ShiftPoll(Shift shift) {
        this.shift = shift;
        this.open = true;
    }

    public ShiftPoll(Account username, Shift shift) {
        this.username = username;
        this.shift = shift;
        this.open = true;
    }

    public ShiftPoll(Time timeDue, Date dateDue, Account username, Shift shift) {
        this.timeDue = timeDue;
        this.dateDue = dateDue;
        this.username = username;
        this.shift = shift;
        this.open = true;
    }

    public Time getTimeDue() {
        return timeDue;
    }

    public void setTimeDue(Time timeDue) {
        this.timeDue = timeDue;
    }

    public Date getDateDue() {
        return dateDue;
    }

    public void setDateDue(Date dateDue) {
        this.dateDue = dateDue;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public List<PollVote> getVotes() {
        return votes;
    }

    public void setVotes(List<PollVote> votes) {
        this.votes = votes;
    }

    public Account getUsername() {
        return username;
    }

    public void setUsername(Account username) {
        this.username = username;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public void addVote(PollVote vote) {
        Objects.requireNonNull(vote);
        if (!votes.contains(vote))
            votes.add(vote);
    }

    public void removeVote(PollVote vote) {
        Objects.requireNonNull(vote);
        votes.remove(vote);
    }

    public void closePoll() {
        this.open = false;
    }

    public Boolean isOpen() {
        return open;
    }

    public Shift getShift() {
        return shift;
    }

    @Override
    public void copy(AbstractEntity toCopy) {
        if (toCopy instanceof ShiftPoll) {
            ShiftPoll newData = (ShiftPoll) toCopy;
            this.timeDue = newData.timeDue;
            this.dateDue = newData.dateDue;
            this.open = newData.open;
            this.username = newData.username;
            this.shift = newData.shift;
        }
    }
}
