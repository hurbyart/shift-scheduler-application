package cz.cvut.fel.shiftScheduler.rest;

import cz.cvut.fel.shiftScheduler.dto.AccountLoginDto;
import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Notification;
import cz.cvut.fel.shiftScheduler.model.Shift;
import cz.cvut.fel.shiftScheduler.service.AccountService;
import cz.cvut.fel.shiftScheduler.service.NotificationService;
import cz.cvut.fel.shiftScheduler.service.ShiftService;
import cz.cvut.fel.shiftScheduler.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController extends BaseController<Account, AccountService>{

    private final ShiftService shiftService;
    private final NotificationService notificationService;

    @Autowired
    public AccountController(AccountService service, ShiftService shiftService, NotificationService notificationService) {
        super(service);
        this.shiftService = shiftService;
        this.notificationService = notificationService;
    }

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> add(@RequestBody Account account){
        if(!service.exists(account.getUsername())){
            if(service.emailExists(account.getEmail())){
                return ResponseEntity.ok("Account with this email is already in database!");
            }

            service.persist(account);
            return ResponseEntity.ok("Account was added");
        }
        return ResponseEntity.ok("Account with this username is already in database!");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody AccountLoginDto accountLoginDto) throws Exception{
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(accountLoginDto.getUsername(), accountLoginDto.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }


        final UserDetails userDetails = service.loadUserByUsername(accountLoginDto.getUsername());
        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(jwt);
    }


    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public ResponseEntity<?> getCurrentUser(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        Account user = service.findByUsername(new JwtUtil().extractUsername(token));
        return ResponseEntity.ok(user);
    }

    @RequestMapping(value = "/current/shifts", method = RequestMethod.GET)
    public List<Shift> getMyShifts(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        Account user = service.findByUsername(new JwtUtil().extractUsername(token));

        return shiftService.findAllByWorker(user);
    }

    @RequestMapping(value = "/current/notifications", method = RequestMethod.GET)
    public List<Notification> getMyNotifications(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        Account user = service.findByUsername(new JwtUtil().extractUsername(token));

        return notificationService.findByAccount(user);
    }

    @RequestMapping(value = "/current/shifts/{shiftId}/cancel", method = RequestMethod.DELETE)
    public ResponseEntity<?> cancelShiftReservation(@PathVariable int shiftId){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        String username = new JwtUtil().extractUsername(token);

        if(service.cancelShiftReservation(username, shiftId)) {
            return ResponseEntity.ok("You were successfully removed from shift.");
        }
        else{
            return ResponseEntity.ok("You can't be removed from shift.");
        }
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "/{workerId}/shifts", method = RequestMethod.GET)
    public List<Shift> getShifts(@PathVariable int workerId){
        Account user = service.find(workerId);
        if(user == null) {
            return null;
        }
        return shiftService.findAllByWorker(user);
    }


}
