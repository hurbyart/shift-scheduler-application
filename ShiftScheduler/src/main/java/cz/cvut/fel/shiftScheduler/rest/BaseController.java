package cz.cvut.fel.shiftScheduler.rest;

import cz.cvut.fel.shiftScheduler.model.AbstractEntity;
import cz.cvut.fel.shiftScheduler.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

public abstract class BaseController<MODELOBJ, SERVICEOBJ> implements GenericController<MODELOBJ, SERVICEOBJ>{

    protected final SERVICEOBJ service;

    @Autowired
    public BaseController(SERVICEOBJ service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<MODELOBJ> getEntities() {
        return ((BaseService<MODELOBJ>) service).findAll();
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createEntity(@RequestBody MODELOBJ entity) {
        ((BaseService<MODELOBJ>) service).persist(entity);
        return ResponseEntity.ok(entity.getClass().getSimpleName() + " was added.");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MODELOBJ getById(@PathVariable("id") Integer id) {
        final MODELOBJ entity = ((BaseService<MODELOBJ>) service).find(id);

        return entity;
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "/{id}/remove", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeEntity(@PathVariable("id") Integer id) {
        final MODELOBJ entity = ((BaseService<MODELOBJ>) service).find(id);
        if (entity != null) {
            ((BaseService<MODELOBJ>) service).remove(entity);
            return ResponseEntity.ok(entity.getClass().getSimpleName() + " was deleted.");
        }
        return ResponseEntity.ok(entity.getClass().getSimpleName() + " was not deleted.");
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@PathVariable("id") Integer id, @RequestBody MODELOBJ newData) {
        MODELOBJ entity = ((BaseService<MODELOBJ>) service).find(id);
        ((AbstractEntity) entity).copy((AbstractEntity) newData);
        ((BaseService<MODELOBJ>) service).update(entity);
        return ResponseEntity.ok(entity.getClass().getSimpleName() + " was updated.");
    }
}
