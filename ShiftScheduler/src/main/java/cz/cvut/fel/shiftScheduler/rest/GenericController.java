package cz.cvut.fel.shiftScheduler.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Base interface for controllers.
 *
 * @param <MODELOBJ>, <SERVICEOBJ>
 */
public interface GenericController<MODELOBJ, SERVICEOBJ> {

    /**
     * Returns every entity instance.
     *
     * @return Every entity instance or {@code null} if no such instance exists
     */
    List<MODELOBJ> getEntities();

    /**
     * Creates the specified entity.
     *
     * @param entity Entity to persist
     */
    ResponseEntity<?> createEntity(@RequestBody MODELOBJ entity);

    /**
     * Finds entity instance with the specified identifier.
     *
     * @param id Identifier
     * @return Entity instance or {@code null} if no such instance exists
     */
    MODELOBJ getById(@PathVariable("id") Integer id);

    /**
     * Removes the specified entity.
     *
     * @param id of entity to remove
     */
    ResponseEntity<?> removeEntity(@PathVariable("id") Integer id);

    /**
     * Updates the specified entity.
     *
     * @param id of entity to update
     * @param newData for updating entity
     */
    ResponseEntity<?> updateById(@PathVariable("id") Integer id, @RequestBody MODELOBJ newData);
}