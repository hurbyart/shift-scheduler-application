package cz.cvut.fel.shiftScheduler.rest;

import cz.cvut.fel.shiftScheduler.model.Notification;
import cz.cvut.fel.shiftScheduler.service.NotificationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notifications")
public class NotificationController extends BaseController<Notification, NotificationService> {
    public NotificationController(NotificationService service) {
        super(service);
    }
}
