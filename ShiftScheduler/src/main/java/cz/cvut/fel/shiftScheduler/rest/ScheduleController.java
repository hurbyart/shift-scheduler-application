package cz.cvut.fel.shiftScheduler.rest;

import cz.cvut.fel.shiftScheduler.model.Schedule;
import cz.cvut.fel.shiftScheduler.model.Shift;
import cz.cvut.fel.shiftScheduler.model.ShiftPoll;
import cz.cvut.fel.shiftScheduler.service.ScheduleService;
import cz.cvut.fel.shiftScheduler.service.ShiftPollService;
import cz.cvut.fel.shiftScheduler.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/schedules")
public class ScheduleController extends BaseController<Schedule, ScheduleService> {

    private ShiftService shiftService;
    private ShiftPollService shiftPollService;

    @Autowired
    public ScheduleController(ScheduleService service, ShiftService shiftService, ShiftPollService shiftPollService) {
        super(service);
        this.shiftService = shiftService;
        this.shiftPollService = shiftPollService;
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "/createFromPolls", method = RequestMethod.POST)
    public Schedule generate(@RequestParam String name, @RequestParam List<Integer> pollIds) {
        List<ShiftPoll> polls = shiftPollService.findListById(pollIds);

        return service.create(name, polls);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "{scheduleId}/addShift", method = RequestMethod.POST)
    public ResponseEntity<?> addShift(@PathVariable int scheduleId, @RequestParam int shiftId) {

        Schedule schedule = service.find(scheduleId);
        Shift shift = shiftService.find(shiftId);

        service.addShift(schedule, shift);

        return ResponseEntity.ok("Shift was successfully added to schedule");
    }


    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "{scheduleId}/removeShift", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeShift(@PathVariable int scheduleId, @RequestParam int shiftId) {

        Schedule schedule = service.find(scheduleId);
        Shift shift = shiftService.find(shiftId);

        if (service.removeShift(schedule, shift)) {
            return ResponseEntity.ok("Shift was successfully removed from schedule");
        }
        return ResponseEntity.ok("Shift was not deleted from schedule");
    }
}
