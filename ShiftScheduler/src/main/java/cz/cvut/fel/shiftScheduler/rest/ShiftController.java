package cz.cvut.fel.shiftScheduler.rest;


import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Shift;
import cz.cvut.fel.shiftScheduler.service.AccountService;
import cz.cvut.fel.shiftScheduler.service.NotificationService;
import cz.cvut.fel.shiftScheduler.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/shifts")
public class ShiftController extends BaseController<Shift, ShiftService> {

    private final AccountService accountService;
    private final NotificationService notificationService;

    @Autowired
    public ShiftController(ShiftService service, AccountService accountService, NotificationService notificationService) {
        super(service);
        this.accountService = accountService;
        this.notificationService = notificationService;
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "{shiftId}/assignWorker", method = RequestMethod.POST)
    public ResponseEntity<?> assignWorker(@PathVariable int shiftId, @RequestParam String workerUsername) {
        Shift assignTo = service.find(shiftId);
        Account assignWho = accountService.findByUsername(workerUsername);

        if (assignTo == null) return ResponseEntity.ok("Shift with given id was not found.");
        if (assignWho == null) return ResponseEntity.ok("Worker with given username was not found.");
        service.assignWorker(assignTo, assignWho);

        return ResponseEntity.ok("Worker was successfully assigned to shift.");
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "{shiftId}/removeWorker", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeWorker(@PathVariable int shiftId, @RequestParam String workerUsername) {

        Shift removeFrom = service.find(shiftId);
        Account removeWho = accountService.findByUsername(workerUsername);

        if (removeFrom == null) return ResponseEntity.ok("Shift with given id was not found.");
        if (removeWho == null) return ResponseEntity.ok("Worker with given username was not found.");


        if (service.removeWorker(removeFrom, removeWho)) {
            return ResponseEntity.ok("Worker was successfully removed from shift.");
        }
        return ResponseEntity.ok("Worker was not assigned to shift.");
    }

}
