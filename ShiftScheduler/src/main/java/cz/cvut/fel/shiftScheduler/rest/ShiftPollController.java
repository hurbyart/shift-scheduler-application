package cz.cvut.fel.shiftScheduler.rest;

import cz.cvut.fel.shiftScheduler.dto.PollVoteDto;
import cz.cvut.fel.shiftScheduler.model.*;
import cz.cvut.fel.shiftScheduler.service.*;
import cz.cvut.fel.shiftScheduler.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Time;
import java.sql.Date;

@RestController
@RequestMapping("/polls")
public class ShiftPollController extends BaseController<ShiftPoll, ShiftPollService> {

    private final AccountService accountService;
    private final ShiftService shiftService;
    private final PollVoteService pollVoteService;
    private final NotificationService notificationService;

    private HttpServletRequest request;

    @Autowired
    public ShiftPollController(ShiftPollService service, AccountService accountService, ShiftService shiftService, PollVoteService pollVoteService, NotificationService notificationService) {
        super(service);
        this.accountService = accountService;
        this.shiftService = shiftService;
        this.pollVoteService = pollVoteService;
        this.notificationService = notificationService;
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "/create/{shiftId}", method = RequestMethod.POST)
    public ResponseEntity<?> createFromShiftWithDate(@PathVariable int shiftId, @RequestParam(required = false) Date date, @RequestParam(required = false) Time time) {
        request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        Shift shift = shiftService.find(shiftId);
        if (shift == null)
            return ResponseEntity.ok("Shift was not found.");

        Account user = getCurrentUser();

        service.createFromDateUserShift(time, date, user, shift);
        return ResponseEntity.ok("Poll was added.");
    }

    @RequestMapping(value = "{pollId}/vote", method = RequestMethod.POST)
    public ResponseEntity<?> vote(@PathVariable int pollId, @RequestBody PollVoteDto voteDTO) {
        request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        Account user = getCurrentUser();

        ShiftPoll poll = service.find(pollId);
        if (poll == null)
            return ResponseEntity.ok("This poll doesn't exist.");
        if (!poll.isOpen())
            return ResponseEntity.ok("This poll is already closed.");
        if (service.findVoteByUsername(poll, user.getUsername()) != null)
            return ResponseEntity.ok("This user has already voted!");

        PollVote vote = new PollVote(voteDTO, user);
        pollVoteService.persist(vote);
        service.acceptVote(poll, vote);

        return ResponseEntity.ok("The vote was accepted.");
    }

    @RequestMapping(value = "{pollId}/unvote", method = RequestMethod.DELETE)
    public ResponseEntity<?> unvote(@PathVariable int pollId) {
        request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        Account user = getCurrentUser();

        ShiftPoll poll = service.find(pollId);
        if (poll == null)
            return ResponseEntity.ok("This poll doesn't exist.");
        if (!poll.isOpen())
            return ResponseEntity.ok("This poll is already closed.");

        PollVote vote = service.findVoteByUsername(poll, user.getUsername());
        service.removeVote(poll, vote);

        return ResponseEntity.ok("The vote was removed.");
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @RequestMapping(value = "{pollId}/close", method = RequestMethod.POST)
    public ResponseEntity<?> close(@PathVariable int pollId) {
        ShiftPoll poll = service.find(pollId);
        if (poll == null)
            return ResponseEntity.ok("This poll doesn't exist.");
        if (!poll.isOpen())
            return ResponseEntity.ok("This poll is already closed.");

        service.closePoll(poll);

        return ResponseEntity.ok("The poll was closed.");
    }

    private Account getCurrentUser() {
        String token = request.getHeader("Authorization").split(" ")[1];
        return accountService.findByUsername(new JwtUtil().extractUsername(token));
    }

}
