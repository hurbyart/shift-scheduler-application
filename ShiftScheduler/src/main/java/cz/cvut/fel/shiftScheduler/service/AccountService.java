package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.AccountDao;
import cz.cvut.fel.shiftScheduler.dao.BaseDao;
import cz.cvut.fel.shiftScheduler.dao.NotificationDao;
import cz.cvut.fel.shiftScheduler.dao.ShiftDao;
import cz.cvut.fel.shiftScheduler.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class AccountService extends BaseService<Account> implements UserDetailsService {

    private AccountDao dao;
    private ShiftDao shiftDao;
    private NotificationDao notificationDao;

    @Autowired
    public AccountService(AccountDao dao, ShiftDao shiftDao, NotificationDao notificationDao) {
        this.dao = dao;
        this.shiftDao = shiftDao;
        this.notificationDao = notificationDao;
    }

    @Override
    protected BaseDao<Account> getPrimaryDao() {
        return dao;
    }

    @Transactional
    public void persist(Account account){
        Objects.requireNonNull(account);
        account.setPassword(new BCryptPasswordEncoder().encode(account.getPassword()));
        dao.persist(account);

    }

    @Transactional
    public boolean exists(String username) {
        return dao.findByUsername(username) != null;
    }

    @Transactional
    public boolean emailExists(String email) {
        return dao.findByEmail(email) != null;
    }


    @Transactional
    public AccountDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account user = dao.findByUsername(username);
        AccountDetails accountDetails = null;
        if (user != null) {
            accountDetails= new AccountDetails(user);
        } else {
            throw new UsernameNotFoundException("User with this username does not exist.");
        }
        return accountDetails;
    }

    @Transactional
    public Account findByUsername(String username){
        Account user = dao.findByUsername(username);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Transactional
    public List<Account> findOnlyWorkers(){
        return dao.findOnlyWorkers();
    }

    @Transactional
    public boolean cancelShiftReservation(String username, Integer shiftId){
        Objects.requireNonNull(username);
        Objects.requireNonNull(shiftId);

        Shift shift = shiftDao.find(shiftId);
        long diffInMillies = Math.abs(shift.getDateStart().getTime() - System.currentTimeMillis());
        long diff = TimeUnit.HOURS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        if(diff < 24) { //less than 24 hours before shift
            return false;
        }

        Account user = dao.findByUsername(username);
        shift.cancelWorker(user);
        shiftDao.update(shift);
        freeSlotNotification(shift);

        return true;
    }

    private void freeSlotNotification(Shift shift) {
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.FREE_SHIFT_SLOT);
        notification.getAccounts().addAll(dao.findOnlyWorkers());
        notification.setHeaderText("Free shift slot");
        notification.setMessageText("There is new free slot on the shift " + shift.getShiftName() + ".");
        notificationDao.persist(notification);
    }

}
