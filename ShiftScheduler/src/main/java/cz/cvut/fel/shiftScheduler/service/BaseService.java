package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.BaseDao;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public abstract class BaseService<T> implements GenericService<T>{

    protected abstract BaseDao<T> getPrimaryDao();

    public T find(Integer id) {
        return getPrimaryDao().find(id);
    }

    public List<T> findAll() {
        return getPrimaryDao().findAll();
    }

    public void persist(T entity) {
        getPrimaryDao().persist(entity);
    }

    public void remove(T entity) {
        getPrimaryDao().remove(entity);
    }

    public void update(T entity) {
        getPrimaryDao().update(entity);
    }
}
