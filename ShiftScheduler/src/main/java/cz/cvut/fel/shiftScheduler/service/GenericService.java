package cz.cvut.fel.shiftScheduler.service;



import java.util.List;

/**
 * Base interface for services.
 *
 * @param <T>
 */
public interface GenericService<T> {



    /**
     * Finds entity instance with the specified identifier.
     *
     * @param id Identifier
     * @return Entity instance or {@code null} if no such instance exists
     */
    T find(Integer id);

    /**
     * Finds all instances of the specified class.
     *
     * @return List of instances, possibly empty
     */
    List<T> findAll();

    /**
     * Persists the specified entity.
     *
     * @param entity Entity to persist
     */
    void persist(T entity);

    /**
     * Removes the specified entity.
     *
     * @param entity Entity to remove
     */
    void remove(T entity);

    /**
     * Updates the specified entity.
     *
     * @param entity Entity to update
     */
    void update(T entity);
}
