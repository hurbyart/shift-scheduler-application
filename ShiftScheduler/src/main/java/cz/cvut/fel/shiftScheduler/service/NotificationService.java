package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.BaseDao;
import cz.cvut.fel.shiftScheduler.dao.NotificationDao;
import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Notification;
import cz.cvut.fel.shiftScheduler.model.PollVote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
public class NotificationService extends BaseService<Notification> {

    private NotificationDao dao;

    @Autowired
    public NotificationService(NotificationDao dao) {
        this.dao = dao;
    }

    @Override
    protected BaseDao<Notification> getPrimaryDao() {
        return dao;
    }


    @Transactional
    public void persist(Notification notification) {
        Objects.requireNonNull(notification);
        dao.persist(notification);

    }

    @Transactional
    public List<Notification> findByAccount(Account account){
        return dao.findByAccount(account);
    }
}
