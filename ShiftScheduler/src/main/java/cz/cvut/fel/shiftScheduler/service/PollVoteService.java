package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.BaseDao;
import cz.cvut.fel.shiftScheduler.dao.PollVoteDao;
import cz.cvut.fel.shiftScheduler.model.PollVote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PollVoteService extends BaseService<PollVote> {

    private PollVoteDao dao;

    @Autowired
    public PollVoteService(PollVoteDao dao) {
        this.dao = dao;
    }

    @Override
    protected BaseDao<PollVote> getPrimaryDao() {
        return dao;
    }


}
