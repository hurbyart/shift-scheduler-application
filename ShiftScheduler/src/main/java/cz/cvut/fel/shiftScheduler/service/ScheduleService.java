package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.BaseDao;
import cz.cvut.fel.shiftScheduler.dao.ScheduleDao;
import cz.cvut.fel.shiftScheduler.dao.ShiftDao;
import cz.cvut.fel.shiftScheduler.model.Notification;
import cz.cvut.fel.shiftScheduler.model.Schedule;
import cz.cvut.fel.shiftScheduler.model.Shift;
import cz.cvut.fel.shiftScheduler.model.ShiftPoll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
public class ScheduleService extends BaseService<Schedule> {

    private ScheduleDao dao;
    private ShiftDao shiftDao;

    @Autowired
    public ScheduleService(ScheduleDao dao, ShiftDao shiftDao) {
        this.dao = dao;
        this.shiftDao = shiftDao;
    }

    @Override
    protected BaseDao<Schedule> getPrimaryDao() {
        return dao;
    }

    @Transactional
    public Schedule create(String name, List<ShiftPoll> polls) {
        Objects.requireNonNull(polls);
        Objects.requireNonNull(name);
        Schedule schedule = new Schedule(name, polls);
        dao.persist(schedule);

        for (ShiftPoll poll : polls) {
            shiftDao.update(poll.getShift());
        }

        return schedule;
    }

    @Transactional
    public void addShift(Schedule schedule, Shift shift) {
        Objects.requireNonNull(shift);
        Objects.requireNonNull(schedule);

        schedule.addShift(shift);

        dao.update(schedule);
    }

    @Transactional
    public boolean removeShift(Schedule schedule, Shift shift) {
        Objects.requireNonNull(shift);
        Objects.requireNonNull(schedule);

        boolean ret = schedule.removeShift(shift);

        dao.update(schedule);
        return ret;
    }
}
