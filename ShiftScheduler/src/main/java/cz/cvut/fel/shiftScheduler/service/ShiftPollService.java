package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.*;
import cz.cvut.fel.shiftScheduler.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Time;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ShiftPollService extends BaseService<ShiftPoll> {

    private ShiftPollDao dao;
    private AccountDao accountDao;
    private NotificationDao notificationDao;

    @Autowired
    public ShiftPollService(ShiftPollDao dao, AccountDao accountDao, NotificationDao notificationDao) {
        this.dao = dao;
        this.accountDao = accountDao;
        this.notificationDao = notificationDao;
    }

    protected BaseDao<ShiftPoll> getPrimaryDao() {
        return dao;
    }

    @Transactional
    public ShiftPoll createFromShift(Shift shift) {
        Objects.requireNonNull(shift);
        final ShiftPoll shiftPoll = new ShiftPoll(shift);
        dao.persist(shiftPoll);
        newPollNotification(shift);
        return shiftPoll;
    }

    @Transactional
    public ShiftPoll createFromUserShift(Account createdBy, Shift shift) {
        Objects.requireNonNull(shift);
        Objects.requireNonNull(createdBy);
        ShiftPoll shiftPoll = new ShiftPoll(createdBy, shift);
        dao.persist(shiftPoll);
        newPollNotification(shift);
        return shiftPoll;
    }

    @Transactional
    public ShiftPoll createFromDateUserShift(Time timeDue, Date dateDue, Account createdBy, Shift shift) {
        Objects.requireNonNull(shift);
        ShiftPoll shiftPoll = new ShiftPoll(timeDue, dateDue, createdBy, shift);
        dao.persist(shiftPoll);
        newPollNotification(shift);
        return shiftPoll;
    }

    @Transactional
    public void acceptVote(ShiftPoll poll, PollVote vote) {
        if (poll.isOpen()) {
            Objects.requireNonNull(poll);
            Objects.requireNonNull(vote);
            poll.addVote(vote);
            dao.update(poll);
        }
    }

    @Transactional
    public void removeVote(ShiftPoll poll, PollVote vote) {
        if (poll.isOpen()) {
            Objects.requireNonNull(poll);
            Objects.requireNonNull(vote);
            poll.removeVote(vote);
            dao.update(poll);
        }
    }

    @Transactional
    public void closePoll(ShiftPoll poll) {
        Objects.requireNonNull(poll);
        poll.closePoll();
        dao.update(poll);
    }

    public PollVote findVoteByUsername(ShiftPoll poll, String username) {
        Objects.requireNonNull(poll);
        Objects.requireNonNull(username);
        return dao.findVoteByUsername(poll, username);
    }

    private void newPollNotification(Shift shift) {
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.NEW_POLL);
        notification.getAccounts().addAll(accountDao.findOnlyWorkers());
        notification.setHeaderText("New poll");
        notification.setMessageText("There is new poll for shift with name  " + shift.getShiftName() + ".");
        notificationDao.persist(notification);
    }

    public List<ShiftPoll> findListById(List<Integer> ids) {
        List<ShiftPoll> ret = new ArrayList<ShiftPoll>();
        for (Integer id : ids) {
            ShiftPoll poll = dao.find(id);
            ret.add(poll);
        }

        return ret;
    }
}
