package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.dao.AccountDao;
import cz.cvut.fel.shiftScheduler.dao.BaseDao;
import cz.cvut.fel.shiftScheduler.dao.NotificationDao;
import cz.cvut.fel.shiftScheduler.dao.ShiftDao;
import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Notification;
import cz.cvut.fel.shiftScheduler.model.NotificationType;
import cz.cvut.fel.shiftScheduler.model.Shift;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ShiftService extends BaseService<Shift> {

    private ShiftDao dao;
    private AccountDao accountDao;
    private NotificationDao notificationDao;

    @Autowired
    public ShiftService(ShiftDao dao, AccountDao accountDao, NotificationDao notificationDao) {
        this.dao = dao;
        this.accountDao = accountDao;
        this.notificationDao = notificationDao;
    }

    @Transactional
    public void persist(Shift shift) {
        Objects.requireNonNull(shift);
        dao.persist(shift);
    }

    @Override
    protected BaseDao<Shift> getPrimaryDao() {
        return dao;
    }

    @Transactional
    public void assignWorker(Shift shift, Account worker) {
        Objects.requireNonNull(shift);
        Objects.requireNonNull(worker);

        shift.assignWorker(worker);
        assignedWorkerNotification(worker, shift);
        dao.update(shift);
    }

    @Transactional
    public boolean removeWorker(Shift shift, Account worker) {
        Objects.requireNonNull(shift);
        Objects.requireNonNull(worker);

        boolean ret = shift.cancelWorker(worker);
        dao.update(shift);
        if (ret) {
            removingWorkerNotification(worker, shift);
            freeSlotNotification(shift);
        }
        return ret;
    }

    @Transactional
    public List<Shift> findAllByWorker(Account worker) {
        return dao.findAllByWorker(worker);
    }

    @Transactional
    public List<Shift> findAllByDate(Date date) {
        return dao.findAllByDate(date);
    }

    @Override
    public void remove(Shift shift) {
        removingShiftNotification(shift);
        getPrimaryDao().remove(shift);
    }

    private void removingWorkerNotification(Account account, Shift shift) {
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.INFORMATION);
        notification.getAccounts().add(account);
        notification.setHeaderText("Removing from shift");
        notification.setMessageText("You were removed from shift " + shift.getShiftName() + ".");
        notificationDao.persist(notification);
    }

    private void assignedWorkerNotification(Account account, Shift shift) {
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.INFORMATION);
        notification.getAccounts().add(account);
        notification.setHeaderText("New shift");
        notification.setMessageText("You were assigned to shift " + shift.getShiftName() + ".");
        notificationDao.persist(notification);
    }

    private void freeSlotNotification(Shift shift) {
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.FREE_SHIFT_SLOT);
        notification.getAccounts().addAll(accountDao.findOnlyWorkers());
        notification.setHeaderText("Free shift slot");
        notification.setMessageText("There is new free slot on the shift " + shift.getShiftName() + ".");
        notificationDao.persist(notification);
    }

    private void removingShiftNotification(Shift shift) {
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.SHIFT_CANCEL);
        notification.getAccounts().addAll(shift.getAssignedAccounts());
        notification.setHeaderText("Shift was removed");
        notification.setMessageText("Shift with name " + shift.getShiftName() + " where you were assigned was removed.");
        notificationDao.persist(notification);
    }
}
