package cz.cvut.fel.shiftScheduler.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class ScheduleTest {

    @Test
    public void createScheduleFromShiftPollsMakesCorrectDates(){
        Date earliestDate = new Date(2020, Calendar.JULY,11);
        Date latestDate = new Date(2020, Calendar.NOVEMBER,15);

        Shift shift1 = new Shift();
        shift1.setDateStart(latestDate);

        Shift shift2 = new Shift();
        shift2.setDateStart(earliestDate);

        Shift shift3 = new Shift();
        shift3.setDateStart(new Date(2020, Calendar.SEPTEMBER,9));

        ShiftPoll shiftPoll1 = new ShiftPoll();
        shiftPoll1.setShift(shift1);

        ShiftPoll shiftPoll2 = new ShiftPoll();
        shiftPoll2.setShift(shift2);

        ShiftPoll shiftPoll3 = new ShiftPoll();
        shiftPoll3.setShift(shift3);

        Schedule schedule = new Schedule("test", Arrays.asList(shiftPoll1,shiftPoll2,shiftPoll3));

        assertEquals(earliestDate, schedule.getFromDate());
        assertEquals(latestDate, schedule.getToDate());

    }
}
