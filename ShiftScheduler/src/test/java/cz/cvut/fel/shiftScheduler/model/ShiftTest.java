package cz.cvut.fel.shiftScheduler.model;


import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ShiftTest {

    @Test
    public void assignWorkersToShiftSavesWorkersInAssignedAccounts() {
        final Shift shift = new Shift();
        shift.setShiftName("shift1");

        Account account1 = new Account();
        account1.setUsername("account1");

        Account account2 = new Account();
        account2.setUsername("account2");

        shift.setAssignedAccounts(Arrays.asList(account1, account2));

        List<Account> assignedAccounts = shift.getAssignedAccounts();

        assertEquals(account1, assignedAccounts.get(0));
        assertEquals(account2, assignedAccounts.get(1));

    }

}