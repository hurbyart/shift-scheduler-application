package cz.cvut.fel.shiftScheduler.service;

import cz.cvut.fel.shiftScheduler.model.Account;
import cz.cvut.fel.shiftScheduler.model.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AccountServiceTest {
    @Autowired
    AccountService accountService;

    @Test
    public void existsFindsAccountAfterPersist(){
        Account worker = new Account();
        worker.setUsername("user");
        worker.setEmail("@@@");
        worker.setFirstName("user");
        worker.setLastName("user");
        worker.setPassword("123");
        worker.setRole(Role.WORKER);

        accountService.persist(worker);

        assertTrue(accountService.exists(worker.getUsername()));
    }
}
