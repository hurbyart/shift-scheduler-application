package cz.cvut.fel.shiftScheduler.service;


import cz.cvut.fel.shiftScheduler.model.Schedule;
import cz.cvut.fel.shiftScheduler.model.Shift;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ScheduleServiceTest {

    @Autowired
    ScheduleService scheduleService;


    @Test
    public void addShiftAddsShiftToSchedule(){
        Schedule schedule = new Schedule();
        schedule.setName("scheduleName");
        schedule.setFromDate(new Date(2020, Calendar.JULY, 9));
        schedule.setToDate(new Date(2020, Calendar.SEPTEMBER, 9));

        Shift shift = new Shift();
        shift.setShiftName("shiftName");
        shift.setDateStart(new Date(2020, Calendar.JULY, 9));
        shift.setFromTime(new Time(22));
        shift.setToTime(new Time(21));

        scheduleService.addShift(schedule, shift);
        assertEquals(shift, schedule.getShifts().get(0));
    }


}